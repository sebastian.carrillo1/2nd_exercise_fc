from classes import lifeGame


if __name__ == '__main__':
    
    game = lifeGame(x_size=50, y_size=50, iterations=1000 ,
                    proportion_Alive_cellsX100=2, proportion_Blive_cellsX100=2 )

    game.plots()