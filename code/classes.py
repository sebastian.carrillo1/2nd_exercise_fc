import numpy as np
import matplotlib.pyplot as plt
import random
import time

plt.rcParams.update({'font.size': 16})

def bi_gauss(x,y):
    '''
    This is a bidimensional normal distribution centered at (0,0)
    and both variances equal to 1.

    For an entry (x,y) it returns its probability density value.
    '''
    p = (1/(2*np.pi))*np.exp(-(x*x+y*y)/2)
    return p

def density_estimation(sample,data,h):
    '''
    This function is the estimated probability density function using 2D gaussian kernels.

    Parameters:
               sample: is a tuple of values (x,y) and the function returns its probability density value.
               data: is an array with shape (2,n) with the data of point (x_i,y_i) to create the 
                     aproximated  probability density function.
               h: is the smoothing parameter, that is analogous to the standar deviation.
    '''
    data = np.transpose(data)
    n = np.shape(data)[0]
    x,y = sample[0],sample[1]
    probability = 0

    for di in data:
        probability += bi_gauss((x-di[0])/h,(y-di[1])/h)
    
    probability = probability/(n*h)
    return probability

class lifeGame():
    
    #Constructor
    def __init__(self, x_size:int, y_size:int, iterations:int ,
                 proportion_Alive_cellsX100:int=3, proportion_Blive_cellsX100:int=2 ) -> None:
        '''
    Initialize the game grid and set up initial configurations for a two-species version of Conway's Game of Life.

    Parameters:
    - x_size (int): The width of the game grid.
    - y_size (int): The height of the game grid.
    - iterations (int): The number of iterations the game will run.
    - proportion_Alive_cellsX100 (int): The scaled percentage of initially alive cells for species A. 
      For example, a value of 3 corresponds to 30% of the cells being alive.
    - proportion_Blive_cellsX100 (int): The scaled percentage of initially alive cells for species B.
      For example, a value of 2 corresponds to 20% of the cells being alive.

    This constructor initializes the game board and sets the initial state with a specified proportion of alive cells
    for two distinct species, A and B, based on provided percentages. Each cell on the board is initially either 
    dead or alive based on these proportions. The species are distributed randomly across the grid.
    '''
        self.x_size = x_size
        self.y_size = y_size
        self.proportion_Alive_cellsX100 = proportion_Alive_cellsX100 
        self.proportion_Blive_cellsX100 = proportion_Blive_cellsX100
        self.iterations = iterations

    #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
    def new_board(self) -> list:
        '''
        Generate a new game board for the initial setup of Conway's Game of Life for two species.

    This method creates a new board where each cell is randomly assigned a state based on the initial proportions
    for alive cells of species A and B or a dead state. The proportions are provided in the constructor.

    The board is represented as a NumPy array, with an additional border of dead cells (zeros) around the actual game area
    to simplify edge case handling during the game evolution.

    Returns:
    - np.ndarray: A NumPy array representing the game board. The array dimensions are (x_size + 2, y_size + 2),
      where `x_size` and `y_size` are the dimensions specified during the object creation. Each cell in the array
      can have one of three possible values:
        - 0: Cell is dead.
        - 1: Cell contains a live individual of species A.
        - 2: Cell contains a live individual of species B.
        '''
        
        board = []
        for i in range(0, self.y_size):
            board.append(
                [random.choice([1] * self.proportion_Alive_cellsX100 + [2] * self.proportion_Blive_cellsX100 + [0] * (10-self.proportion_Alive_cellsX100-self.proportion_Blive_cellsX100 )) for _ in range(0, self.x_size)]
                )
        full_board = np.zeros(shape=(self.x_size+2,self.y_size+2))
        full_board[1:-1,1:-1] = np.array(board)
        
        return full_board
    
    #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    def assign(self, board:np.ndarray, x_position:int, y_position:int, value:int):
        '''
        Assigns a value at location (x, y) on a board, wrapping around if out-of-bounds
        '''
        
        board[x_position,y_position] = value
        
    #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    def count_neighbors(self,board, x_position:int, y_position:int) -> tuple:
        '''
        Count the number of living neighbors for two species around a specified cell on the board.

    This method determines how many neighboring cells contain living members of each species. It considers
    all eight adjacent cells around the specified position (x_position, y_position) on the board.

    Parameters:
    - board (np.ndarray): The game board, which is a 2D NumPy array where cells are marked as 0 for dead,
      1 for a living individual of species A, and 2 for a living individual of species B.
    - x_position (int): The x-coordinate (horizontal position) of the cell whose neighbors are to be counted.
    - y_position (int): The y-coordinate (vertical position) of the cell whose neighbors are to be counted.

    Returns:
    - tuple: A tuple (nNeighborA, nNeighborB) where nNeighborA is the number of living neighbors of species A
      and nNeighborB is the number of living neighbors of species B.
        '''
        x = x_position
        y = y_position
         
        A:list = []
        B:list = []
        
        (A if (board[ x - 1, y ] == 1) else B).append(board[ x - 1, y ])
        (A if (board[ x + 1, y ] == 1) else B).append(board[ x + 1, y ])
        (A if (board[ x, y - 1 ] == 1) else B).append(board[ x, y - 1 ])
        (A if (board[ x, y + 1 ] == 1) else B).append(board[ x, y + 1 ])
        
        (A if (board[ x + 1, y + 1 ] == 1) else B).append(board[ x + 1, y + 1 ])
        (A if (board[ x + 1, y - 1 ] == 1) else B).append(board[ x + 1, y - 1 ])
        (A if (board[ x - 1, y + 1 ] == 1) else B).append(board[ x - 1, y + 1 ])
        (A if (board[ x - 1, y - 1 ] == 1) else B).append(board[ x - 1, y - 1 ])
        
        nNeighborA:int = int(sum(A)) 
        nNeighborB:int = np.count_nonzero(np.array(B)) 

        
        return nNeighborA, nNeighborB
    #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    def process_life(self, board:np.ndarray ) -> np.ndarray:
        '''
         Advance the current state of Conway's Game of Life to the next iteration.

         This method applies the rules of Conway's Game of Life to the current state of the board
         to generate the next iteration. The rules dictate whether cells survive, die, or are born
         based on the number of neighboring cells belonging to each species.

         Parameters:
         - board (np.ndarray): The current state of the game board, represented as a 2D NumPy array.
           Each cell in the array can have one of three possible values
           
        Returns:
         - np.ndarray: The state of the game board after applying the rules to advance to the next iteration.
        '''
    
        next_board = np.zeros(shape=(self.x_size+2,self.y_size+2))

        for y in range(1, self.x_size):

            for x in range(1, self.y_size):
                
                NA, NB = self.count_neighbors(board, x, y)

                is_A_alive = board[x, y] == 1
                is_B_alive = board[x, y] == 2

                #Surviving condition
                
                if ( (is_A_alive) and (2<=NA<=3)  and (NB<2) ):
                    self.assign(next_board, x, y, 1) 
                elif ( (is_B_alive) and (2<=NB<=3) and (NA<2) ):
                    self.assign(next_board, x, y, 2)
                
                #Death condition
                elif ( (is_A_alive) and ( (3<NA or NA<2) or NB>2) ):
                    self.assign(next_board, x, y, 0) 
                    
                elif ( (is_B_alive) and ( (3<NB or NB<2) or NA>2) ):
                    self.assign(next_board, x, y, 0)
                
                #Birth condition
                elif (not is_A_alive) and ( 3<=NA<=5 and NB<=2) :
                    self.assign(next_board, x, y, 1)
                elif (not is_B_alive) and ( 3<=NB<=5 and NA<=2):
                    self.assign(next_board, x, y, 2)
                
                else:
                    self.assign(next_board, x, y, 0)       

        return next_board
    #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
    def draw_board(self, board:np.ndarray):
        '''
        Display the current state of the game board in the console.

        This method visually represents the current state of the game board in the console.
        Each cell in the board is represented by a character indicating the species it belongs to

        Parameters:
        - board (np.ndarray): The current state of the game board, represented as a 2D NumPy array.

        Returns:
        - str: A string representation of the game board for display in the console.

        '''
        res = ''
        for row in board:
            for col in row:
                if col == 1.:
                    res += '1 '
                elif col == 2.:
                    res += '2 '
                else:
                    res += '  '
            res += '\n'
        return res
    #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    def runGame(self, value = False) -> None:
        '''
        Runs Conway's Game of Life.

        This method starts the simulation by generating the initial game board and running iterations
        of Conway's Game of Life based on the specified number of iterations. Optionally, it can return
        the number of living individuals of each species for each iteration.

        Parameters:
        - value (bool, optional): If True, the method returns the number of living individuals of each species
          for each iteration. Default is False.

        Returns:
        - None: If `value` is False, the method prints the board state for each iteration in the console
          and does not return any value.
        - Tuple[np.ndarray, np.ndarray]: If `value` is True, the method returns two NumPy arrays representing
          the number of living individuals of species A and B, respectively, for each iteration.
        '''
        board = self.new_board()

        NUM_ITERATIONS = self.iterations
        
        if not value:
            for i in range(0, NUM_ITERATIONS):
                print('Iteration ' + str(i + 1))
           
                board = self.process_life(board)
                res = self.draw_board(board)
            
                print(res)
                time.sleep(0.4)
            
        if value:
            res = self.draw_board(board)
            
            amountLiveAByIter = np.zeros(NUM_ITERATIONS)
            amountLiveBByIter = np.zeros(NUM_ITERATIONS)
            
            for i in range(0, NUM_ITERATIONS):
                
                amountLiveAByIter[i] = res.count('1')
                amountLiveBByIter[i] = res.count('2')
                
                board = self.process_life(board)
                res = self.draw_board(board)
            
            return amountLiveAByIter, amountLiveBByIter
    #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    def plots(self):
        '''
        Generate plots to visualize the static results of the Conway's Game of Life simulation and Metropolis-Hastings algorithm.

        This method generates plots to visualize the evolution of the game board over multiple iterations and compares
        it with the results obtained from the Metropolis-Hastings algorithm. The plots include:
        - Bar plots showing the percentage of occupied cells for species A and B over each iteration.
        - A 2D histogram representing the distribution of occupied cells for both species across all the iterations.
        - Another 2D histogram showing the results of the Metropolis algorithm, simulating the same distribution
          as obtained from the game simulation.

        Returns:
        - None: The plots are saved as image files and not returned as objects.
        '''
        notNormalizedCounts = self.runGame(value=True)

        data = np.array(notNormalizedCounts)/(self.x_size*self.y_size)
        n = self.iterations
        x_bar = np.arange(1,n+1)

        #----------------------------------------------------------------------------------------------------

        fig1, axes = plt.subplot_mosaic([['A','A','C','C'],
                                         ['B','B','C','C']], figsize= (20,10))
        
        axes['A'].bar(x_bar,data[0],linewidth=0.,width=1.)
        axes['B'].bar(x_bar,data[1],linewidth=0.,width=1.)
        axes['C'].hist2d(data[0], data[1],
                         bins=150, cmap='plasma', cmin=0.1)
        
        axes['A'].set_title('Ocupación del tablero de la especie A')
        axes['A'].set_xlabel('Iteración')
        axes['A'].set_ylabel('Porcentaje de ocupación')
        axes['A'].set_xlim(1,n)

        axes['B'].set_title('Ocupación del tablero de la especie B')
        axes['B'].set_xlabel('Iteración')
        axes['B'].set_ylabel('Porcentaje de ocupación')
        axes['B'].set_xlim(1,n)

        axes['C'].set_title('Histograma del juego de la vida')
        axes['C'].set_xlabel('Porcentaje de ocupación especie A')
        axes['C'].set_ylabel('Porcentaje de ocupación especie B')
        
        fig1.tight_layout()

        plt.savefig('./Histograms.png')
        plt.close()
        #----------------------------------------------------------------------------------------------------
        cov_matrix = np.cov(data)
        mean_std = np.mean([cov_matrix[0,0],cov_matrix[1,1]])
        x_min, y_min = np.min(data[0]),np.min(data[1])
        delta_x = np.max(data[0])-x_min
        delta_y = np.max(data[1])-y_min

        phi = [[delta_x*np.random.random()+x_min,
                delta_y*np.random.random()+y_min]]
        
        while len(phi)<n:
            phi_p = [delta_x*np.random.random()+x_min,
                     delta_y*np.random.random()+y_min]
            
            alpha = density_estimation(phi_p,data,mean_std)/density_estimation(phi[-1],data,mean_std)
            d_S = -np.log(alpha)

            if d_S<0:
              phi.append(phi_p)

            elif d_S>0:
              x = np.random.random()

              if x<alpha:
                phi.append(phi_p)

              elif x>alpha:
                continue
            
        simulation = np.transpose(np.array(phi))

        fig2, axes = plt.subplot_mosaic([['A','A','C','C'],
                                         ['A','A','C','C']], figsize= (20,10))
        
        axes['A'].hist2d(data[0], data[1],
                    bins=150, cmap='plasma', cmin=0.1)
        
        axes['C'].hist2d(simulation[0], simulation[1],
                    bins=150, cmap='plasma', cmin=0.1)
        
        axes['A'].set_title('Histograma del juego de la vida')
        axes['C'].set_title('Histograma de metrópolis')

        axes['A'].set_xlabel('Porcentaje de ocupación especie A')
        axes['A'].set_ylabel('Porcentaje de ocupación especie B')

        axes['C'].set_xlabel('Porcentaje de ocupación especie A')
        axes['C'].set_ylabel('Porcentaje de ocupación especie B')

        fig2.tight_layout()

        plt.savefig('./Metropolis.png')
        plt.close()