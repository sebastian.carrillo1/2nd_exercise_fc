# 2nd_exercise_FC



## Authors 
- [Santiago Ramirez Gaviria.](https://gitlab.com/santiago.ramirez3)
  
santiago.ramirez3@udea.edu.co
- [Sebastian Carrillo Mejia.](https://gitlab.com/sebastian.carrillo1)
  
sebastian.carrillo1@udea.edu.co

# Extended Game of Life

This project extends the classic Conway's Game of Life by introducing two distinct species of cells competing for survival on a 50 by 50 grid distribution. Each species has its own rules for survival, reproduction, and death, influenced by the density of their kind and the opposing species.

## Overview

The Extended Game of Life features:
- A grid where each cell can either be empty, occupied by species A, or occupied by species B.
- Equal survival and reproduction rules for each species based on the local population densities of A and B around each cell.
- The ability to simulate the evolution of the grid over multiple iterations and visualize the outcome.
- Statistical analysis capabilities to monitor population changes over time carried out by plotting the joint probability distribution and the counts of amount of live especies individuals per iteration.
- Also, the Kernel aproximation method was used to generate points that follow the same distribution of the results of the game, using the metroplis algorithm.


## Conditions for Survival, Death, and Birth

The rules for each iteration for species A and B are as follows:

### Survival:
- **Species A** survives if is alive and:
  $$
  (2 \leq N_A \leq 3) \quad \text{and} \quad (N_B < 2)
  $$
- **Species B** survives if is alive and:
  $$
  (2 \leq N_B \leq 3) \quad \text{and} \quad (N_A < 2)
  $$

### Death:
- **Species A** dies if is alive and:
  $$
  (N_A > 3 \quad \text{or} \quad N_A < 2) \quad \text{or} \quad (N_B > 2)
  $$
- **Species B** dies if is alive and:
  $$
  (N_B > 3 \quad \text{or} \quad N_B < 2) \quad \text{or} \quad (N_A > 2)
  $$

### Birth:
- A new cell of **Species A** is born if is dead and:
  $$
  (3 \leq N_A \leq 5\quad \text{and} \quad N_B \leq 2) \quad 
  $$
- A new cell of **Species B** is born if is dead and:
  $$
  (3 \leq N_B \leq 5 \quad \text{and} \quad N_A \leq 2) \quad 
  $$
  As you can see, the original conway's life game conditions were changed, allowing, the  birth condition to be satisfied more frequently


## Special annotations

While running the code we had some issues with the metropolis algorithm, when evaluating, we got the expression -$\frac{\infty}{0}$.

This problem happened because we found a joint density probability, which corresponds to a discrete distribution, allowing some values to have 0 probability of occurrence, causing the previous expression to appear in the metropolis algorithm.

The problem was solved, considering that we can find a approximated continuum distribution using the density probability approximation with kernel, utilizing a 2D gaussian distribution for each obtained value in the life game results.

This method, allowed us to define a non-zero probability for all interest interval, relating low probability occurrence for some points, that had 0 probability of occurrence in the discrete distibution

This is however a brief explanation of the motivation for implementing this method, if interested in delving into the method precise steps, please rely to the following documentation.

https://en.wikipedia.org/wiki/Kernel_density_estimation

## Run Locally 
First, create a python virtual enviroment and activate it.

```bash
virtualenv venv
source venv/bin/activate
```


get inside the virtual enviroment, add a new empty repo and inside it, clone using:

```bash
  git clone https://https://gitlab.com/sebastian.carrillo1/2nd_excersise_fc
```

Go to the project directory (if you're not inside already) and install dependencies from requirements.txt

```bash
  cd 2nd_excersise_fc
  pip install -r requirements.txt
```

## Usage
To run this code, all you need is move to code directory and execute the execution.py file, after that, the game will ran and create the desired plots, however, here'sa brief explanation of the process that each file does, if need more information you can rely to the source code and check the docstrings.

- execution.py:
This file, imports classes, to create a object from the class lifeGame, from which, executing one simple method, we obtain  the desired results.

- classes.py:
This file, contains the class lifeGame  and some auxiliar functions that allow us to implement the metropolis algorithm using the distribution approximation for the kernel method

    An object of the class, corresponds to a single game, for which, we can define the size of the board (grid), the amount of iterations, and the proportion of initial alive individuals of A and B species.

## Support
If having any trouble or bugg while runnig the code, please don't hesitate and send us an e-mail, also, new recomendations for improvements are appreciated.

Contributors e-mail can be found in the Authors section.
